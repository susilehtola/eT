


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        cc2
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          diis
        singlet states:     4
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
        right eigenvectors
     end solver cc es


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               117
     Significant AO pairs:                  430

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               405 /     107       0.47383E+01         147             42             17010
     2               318 /      92       0.47165E-01         234            111             35298
     3               246 /      74       0.46944E-03         178            183             45018
     4               173 /      51       0.38270E-05         145            265             45845
     5                70 /      18       0.38106E-07          78            324             22680
     6                 0 /       0       0.37202E-09          33            345                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 345

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.6608E-11
     Minimal element of difference between approximate and actual diagonal:  -0.8882E-15

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10200
     Total cpu time (sec):               0.15979


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7380E-10
    10           -78.843851693631     0.1594E-07     0.3979E-12
    11           -78.843851693631     0.3322E-08     0.1421E-13
    12           -78.843851693631     0.1197E-08     0.0000E+00
    13           -78.843851693631     0.4264E-09     0.5684E-13
    14           -78.843851693631     0.1280E-09     0.8527E-13
    15           -78.843851693631     0.1479E-10     0.4263E-13
    16           -78.843851693631     0.2905E-11     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195145   9   0.704416610867  17   1.682695299877  25   3.175416260660
   2  -1.277667044968  10   0.746260122914  18   1.804186820052  26   3.209661156313
   3  -0.898849406810  11   1.155862024383  19   1.902641648742  27   3.328173215200
   4  -0.629870222690  12   1.170770887076  20   2.148883457168  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756969  29   3.985492632586
   6  -0.485872762832  14   1.449847537220  22   2.540321860021
   7   0.159756317416  15   1.463234913438  23   2.541517310547
   8   0.229311906283  16   1.474444394527  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.45500
     Total cpu time (sec):               0.77590


  :: CC2 wavefunction
  ======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (diis algorithm)


  1) Preparation of MO basis and integrals

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.3734E-01     0.7908E+02
    2           -79.085431671928     0.7567E-02     0.1190E-02
    3           -79.085759807020     0.9940E-03     0.3281E-03
    4           -79.085768585649     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729229     0.3646E-06     0.6526E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729585     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1073E-10
   13           -79.085769729575     0.4926E-10     0.4547E-12
   14           -79.085769729575     0.6368E-11     0.3979E-12
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729575

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5       -0.015071367766
       14      4        0.009516336779
        7      4       -0.008547796583
       15      5        0.006232215147
        5      6        0.005875107063
        6      2       -0.005220429230
       13      5       -0.005212699581
        2      4       -0.005071084622
       11      6       -0.003616248565
        4      5       -0.003233309227
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007125912546

  - Finished solving the CC2 ground state equations

     Total wall time (sec):              0.11100
     Total cpu time (sec):               0.21246


  3) Calculation of the excited state (diis algorithm)
     Calculating right vectors

   - DIIS coupled cluster excited state solver
  -----------------------------------------------

  A DIIS solver that solves for the lowest eigenvalues and  the right 
  eigenvectors of the Jacobian matrix, A. The eigenvalue  problem is solved 
  by DIIS extrapolation of residuals for each  eigenvector until the convergence 
  criteria are met.

  More on the DIIS algorithm can be found in P. Pulay, Chemical Physics 
  Letters, 73(2), 393-398 (1980).

  - Settings for coupled cluster excited state solver (DIIS):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               4
     Max number of iterations:             100

     DIIS dimension:                        20

  Iteration:                  1

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.375015269479       0.4812E+00
     2      0.480641106119       0.4579E+00
     3      0.446772629056       0.4843E+00
     4      0.534881048817       0.4555E+00
  -----------------------------------------------

  Iteration:                  2

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.244778435474       0.5849E-01
     2      0.355881037087       0.7300E-01
     3      0.312223403345       0.7370E-01
     4      0.421045852967       0.6588E-01
  -----------------------------------------------

  Iteration:                  3

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.246587205857       0.1583E-01
     2      0.355638260082       0.2673E-01
     3      0.313511935381       0.2187E-01
     4      0.420777630535       0.2547E-01
  -----------------------------------------------

  Iteration:                  4

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245915847886       0.8217E-02
     2      0.354353445892       0.1334E-01
     3      0.312521885156       0.1280E-01
     4      0.419488174717       0.1496E-01
  -----------------------------------------------

  Iteration:                  5

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245989054591       0.9753E-03
     2      0.354173395183       0.5362E-02
     3      0.312504173773       0.1622E-02
     4      0.418962299736       0.8857E-02
  -----------------------------------------------

  Iteration:                  6

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245984620059       0.3158E-03
     2      0.354228449852       0.4942E-02
     3      0.312496612999       0.5309E-03
     4      0.418421229797       0.7902E-02
  -----------------------------------------------

  Iteration:                  7

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983244402       0.1605E-03
     2      0.354329332013       0.3024E-02
     3      0.312492853298       0.2145E-03
     4      0.418240449755       0.8072E-02
  -----------------------------------------------

  Iteration:                  8

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245984342481       0.1078E-03
     2      0.354253200152       0.9006E-03
     3      0.312492894370       0.2143E-03
     4      0.418221829868       0.5534E-02
  -----------------------------------------------

  Iteration:                  9

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983235671       0.2944E-04
     2      0.354262674264       0.3658E-03
     3      0.312494142404       0.1918E-03
     4      0.418076057835       0.4667E-02
  -----------------------------------------------

  Iteration:                 10

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983340664       0.9824E-05
     2      0.354257500997       0.1133E-03
     3      0.312492010174       0.1322E-03
     4      0.418086024665       0.4495E-02
  -----------------------------------------------

  Iteration:                 11

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983453967       0.5003E-05
     2      0.354257056002       0.7806E-04
     3      0.312492871638       0.5499E-04
     4      0.418034065459       0.1245E-02
  -----------------------------------------------

  Iteration:                 12

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983434971       0.3839E-05
     2      0.354257059690       0.7799E-04
     3      0.312493192561       0.1420E-04
     4      0.418025847647       0.4151E-03
  -----------------------------------------------

  Iteration:                 13

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983449667       0.1173E-05
     2      0.354257499051       0.5715E-04
     3      0.312493123948       0.9326E-05
     4      0.418026030905       0.1534E-03
  -----------------------------------------------

  Iteration:                 14

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441656       0.2628E-06
     2      0.354257136612       0.2442E-04
     3      0.312493158371       0.5079E-05
     4      0.418025633332       0.8621E-04
  -----------------------------------------------

  Iteration:                 15

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441213       0.9712E-07
     2      0.354257061051       0.1519E-04
     3      0.312493144941       0.3333E-05
     4      0.418025874296       0.8542E-04
  -----------------------------------------------

  Iteration:                 16

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441275       0.3517E-07
     2      0.354257057981       0.1080E-04
     3      0.312493136941       0.2539E-05
     4      0.418025260684       0.4673E-04
  -----------------------------------------------

  Iteration:                 17

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441315       0.1116E-07
     2      0.354257046082       0.4455E-05
     3      0.312493144862       0.8058E-06
     4      0.418025446828       0.1090E-04
  -----------------------------------------------

  Iteration:                 18

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441347       0.3808E-08
     2      0.354257078238       0.2123E-05
     3      0.312493140729       0.2909E-06
     4      0.418025352235       0.4411E-05
  -----------------------------------------------

  Iteration:                 19

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1187E-08
     2      0.354257068075       0.1220E-05
     3      0.312493140829       0.8025E-07
     4      0.418025341393       0.1919E-05
  -----------------------------------------------

  Iteration:                 20

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.4394E-09
     2      0.354257077681       0.4694E-06
     3      0.312493140809       0.2428E-07
     4      0.418025355261       0.6588E-06
  -----------------------------------------------

  Iteration:                 21

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.1974E-09
     2      0.354257073797       0.1104E-06
     3      0.312493140804       0.6738E-08
     4      0.418025354415       0.2204E-06
  -----------------------------------------------

  Iteration:                 22

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.9336E-10
     2      0.354257072883       0.2971E-07
     3      0.312493140839       0.2176E-08
     4      0.418025354396       0.7248E-07
  -----------------------------------------------

  Iteration:                 23

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.3384E-10
     2      0.354257072961       0.6032E-08
     3      0.312493140824       0.4547E-09
     4      0.418025354707       0.3221E-07
  -----------------------------------------------

  Iteration:                 24

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072906       0.2223E-08
     3      0.312493140824       0.1140E-09
     4      0.418025354495       0.1116E-07
  -----------------------------------------------

  Iteration:                 25

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072916       0.7741E-09
     3      0.312493140824       0.4018E-10
     4      0.418025354503       0.2903E-08
  -----------------------------------------------

  Iteration:                 26

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072918       0.2783E-09
     3      0.312493140824       0.1156E-10
     4      0.418025354493       0.8194E-09
  -----------------------------------------------

  Iteration:                 27

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072916       0.6710E-10
     3      0.312493140824       0.4178E-11
     4      0.418025354494       0.2237E-09
  -----------------------------------------------

  Iteration:                 28

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072916       0.1844E-10
     3      0.312493140824       0.4178E-11
     4      0.418025354496       0.6011E-10
  -----------------------------------------------

  Iteration:                 29

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072916       0.4595E-11
     3      0.312493140824       0.4178E-11
     4      0.418025354496       0.1856E-10
  -----------------------------------------------

  Iteration:                 30

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1      0.245983441338       0.7173E-11
     2      0.354257072916       0.4595E-11
     3      0.312493140824       0.4178E-11
     4      0.418025354496       0.7429E-11
  -----------------------------------------------
  Convergence criterion met in 30 iterations!

  - Resorting roots according to excitation energy.

  - Stored converged states to file.

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.245983441338
     Fraction singles (|R1|/|R|):       0.980304305798

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.973021602897
        4      6        0.106789400087
        6      6        0.036115666379
       13      6       -0.029969632228
        1      3       -0.011639320545
       22      6        0.007968761285
        1      5        0.007878549025
       19      5       -0.007714058164
        9      6        0.007315563576
       20      4        0.006976576961
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312493140824
     Fraction singles (|R1|/|R|):       0.981441033025

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.965628997075
        3      6        0.158512867306
        7      6       -0.070134692772
       14      6        0.021348644735
        2      3       -0.011607275500
       12      6       -0.006210517823
        2      5        0.005382310312
        8      2        0.005308524736
        5      4        0.004510972655
       11      4        0.003272191867
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.354257072916
     Fraction singles (|R1|/|R|):       0.983257567056

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5        0.965285743630
        2      4       -0.132449574674
        4      5        0.076819133269
        1      2        0.068258196359
        3      4       -0.051829802688
        5      6        0.039876044708
       13      5       -0.028798153835
        7      4        0.026087665160
        4      2        0.018477884696
       10      5        0.013058391632
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.418025354496
     Fraction singles (|R1|/|R|):       0.982264305191

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      5        0.947047794657
        1      4       -0.214995854391
        3      5        0.108409956416
        7      5       -0.069257336150
        4      4       -0.060000555564
        2      2        0.023818797678
        8      6        0.015066880567
       15      4       -0.012597150541
       20      6       -0.010167354110
       14      5        0.009501846973
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.245983441338        6.693550376778
        2                  0.312493140824        8.503371483564
        3                  0.354257072916        9.639825961430
        4                  0.418025354496       11.375049287337
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CC2 excited state equations (right)

     Total wall time (sec):              0.58900
     Total cpu time (sec):               1.25168

  - Timings for the CC2 excited state calculation

     Total wall time (sec):              0.70600
     Total cpu time (sec):               1.47281

  eT terminated successfully!
