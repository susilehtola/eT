#!/usr/bin/env python3

# provides os.path.join
import os

# provides exit
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
f = [
    get_filter(from_string='State                (Hartree)             (eV)',
               num_lines=4,
               abs_tolerance=1.0e-8,
               mask=[2]), 
    get_filter(from_string='Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >',
               num_lines=5,
               abs_tolerance=1.0e-8,
               mask=[1,2],
               ignore_sign=True),
    get_filter(from_string='Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >',
               num_lines=5,
               abs_tolerance=1.0e-8,
               mask=[3]),
    get_filter(string='Oscillator strength [a.u.]:',
               abs_tolerance=1.0e-8),
    get_filter(string='Final ground state energy (a.u.):',
               abs_tolerance=1.0e-8),
    get_filter(string='Convergence criterion met in',
               abs_tolerance=1.0e-8)
]

# invoke the command line interface parser which returns options
options = cli()

ierr = 0
for inp in ['ccsd_oscillator_strength_eom_crop.inp']:
    # the run function runs the code and filters the outputs
    ierr += run(options,
                configure,
                input_files=inp,
                filters={'out': f})

sys.exit(ierr)
