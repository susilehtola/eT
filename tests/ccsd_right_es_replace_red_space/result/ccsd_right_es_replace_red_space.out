


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
        max reduced dimension: 20
     end solver cc es


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10900
     Total cpu time (sec):               0.17188


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7380E-10
    10           -78.843851693631     0.1594E-07     0.3553E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.5684E-13
    13           -78.843851693631     0.4264E-09     0.5684E-13
    14           -78.843851693631     0.1280E-09     0.0000E+00
    15           -78.843851693631     0.1479E-10     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195128   9   0.704416610872  17   1.682695299885  25   3.175416260672
   2  -1.277667044958  10   0.746260122921  18   1.804186820060  26   3.209661156325
   3  -0.898849406810  11   1.155862024397  19   1.902641648752  27   3.328173215213
   4  -0.629870222684  12   1.170770887084  20   2.148883457177  28   3.721936473482
   5  -0.541641772841  13   1.267961746557  21   2.200395756979  29   3.985492632598
   6  -0.485872762812  14   1.449847537230  22   2.540321860021
   7   0.159756317424  15   1.463234913441  23   2.541517310547
   8   0.229311906285  16   1.474444394533  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.41900
     Total cpu time (sec):               0.70594


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       True


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-09
     Energy threshold:          0.10E-09

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.9391E-01     0.7908E+02
    2           -79.092586666608     0.2720E-01     0.8345E-02
    3           -79.099408028306     0.7507E-02     0.6821E-02
    4           -79.100345871310     0.2095E-02     0.9378E-03
    5           -79.100371860301     0.5154E-03     0.2599E-04
    6           -79.100393801016     0.2313E-03     0.2194E-04
    7           -79.100385611260     0.4933E-04     0.8190E-05
    8           -79.100384217524     0.1180E-04     0.1394E-05
    9           -79.100383621795     0.4135E-05     0.5957E-06
   10           -79.100383427012     0.1779E-05     0.1948E-06
   11           -79.100383466393     0.6705E-06     0.3938E-07
   12           -79.100383474630     0.2989E-06     0.8237E-08
   13           -79.100383487352     0.1028E-06     0.1272E-07
   14           -79.100383481865     0.3056E-07     0.5487E-08
   15           -79.100383481092     0.6508E-08     0.7730E-09
   16           -79.100383481303     0.2112E-08     0.2109E-09
   17           -79.100383481485     0.6588E-09     0.1819E-09
   18           -79.100383481546     0.1594E-09     0.6158E-10
   19           -79.100383481554     0.5326E-10     0.7915E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481554

     Correlation energy (a.u.):           -0.256531787923

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5       -0.014740597486
       14      4        0.009546856216
        7      4       -0.008284826477
       15      5        0.006124828872
        4      5       -0.005606072685
        6      2       -0.005476844297
        2      4       -0.005318591723
       13      5       -0.005269818340
        5      6        0.004933006894
       11      6       -0.003454309400
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574399
        9      3       9      3       -0.041367012251
        3      4       3      4       -0.036659067518
        6      5       6      5       -0.034554012169
        1      5       1      5       -0.034177347752
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553602
       18      3      18      3       -0.031351828683
        2      4       3      4       -0.029701270697
     --------------------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007226152104

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.30500
     Total cpu time (sec):               0.88994


  3) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-09
     Residual threshold:              0.10E-09

     Number of singlet states:               2
     Max number of iterations:             100

     Max reduced space dimension:           20

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.378021731473    0.000000000000     0.4419E+00   0.3780E+00
     2   0.485447187524    0.000000000000     0.4256E+00   0.4854E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247587893297    0.000000000000     0.8953E-01   0.1304E+00
     2   0.361637524657    0.000000000000     0.1093E+00   0.1238E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246599742343    0.000000000000     0.3235E-01   0.9882E-03
     2   0.355506758363    0.000000000000     0.4270E-01   0.6131E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247219896514    0.000000000000     0.9662E-02   0.6202E-03
     2   0.356567867806    0.000000000000     0.1455E-01   0.1061E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247240765515    0.000000000000     0.1981E-02   0.2087E-04
     2   0.356116428174    0.000000000000     0.3900E-02   0.4514E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247195004633    0.000000000000     0.5500E-03   0.4576E-04
     2   0.356092222391    0.000000000000     0.1597E-02   0.2421E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247193433574    0.000000000000     0.1674E-03   0.1571E-05
     2   0.356078256948    0.000000000000     0.5745E-03   0.1397E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194072581    0.000000000000     0.5920E-04   0.6390E-06
     2   0.356090221756    0.000000000000     0.2513E-03   0.1196E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194227856    0.000000000000     0.3833E-04   0.1553E-06
     2   0.356076775058    0.000000000000     0.4949E-03   0.1345E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194227856    0.000000000000     0.3833E-04   0.2776E-15
     2   0.356076775058    0.000000000000     0.4949E-03   0.1610E-14
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194213284    0.000000000000     0.2252E-04   0.1457E-07
     2   0.356089976466    0.000000000000     0.4027E-03   0.1320E-04
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194237477    0.000000000000     0.2033E-04   0.2419E-07
     2   0.355972639924    0.000000000000     0.7151E-01   0.1173E-03
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194227083    0.000000000000     0.1025E-04   0.1039E-07
     2   0.317456730017    0.000000000000     0.9656E-01   0.3852E-01
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194276940    0.000000000000     0.3622E-05   0.4986E-07
     2   0.312790073755    0.000000000000     0.3283E-01   0.4667E-02
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194264494    0.000000000000     0.1395E-05   0.1245E-07
     2   0.312911583024    0.000000000000     0.1393E-01   0.1215E-03
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194251472    0.000000000000     0.5169E-06   0.1302E-07
     2   0.312843347820    0.000000000000     0.5535E-02   0.6824E-04
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194253150    0.000000000000     0.2731E-06   0.1679E-08
     2   0.312719632961    0.000000000000     0.3487E-02   0.1237E-03
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194256269    0.000000000000     0.1135E-06   0.3119E-08
     2   0.312710501613    0.000000000000     0.1837E-02   0.9131E-05
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194256269    0.000000000000     0.1135E-06   0.2498E-15
     2   0.312710501613    0.000000000000     0.1837E-02   0.1832E-14
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255788    0.000000000000     0.4121E-07   0.4809E-09
     2   0.312718577659    0.000000000000     0.7348E-03   0.8076E-05
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255496    0.000000000000     0.1973E-07   0.2927E-09
     2   0.312722787554    0.000000000000     0.4161E-03   0.4210E-05
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255495    0.000000000000     0.8400E-08   0.5740E-12
     2   0.312714934467    0.000000000000     0.2428E-03   0.7853E-05
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255585    0.000000000000     0.2953E-08   0.8986E-10
     2   0.312716132534    0.000000000000     0.1038E-03   0.1198E-05
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1184E-08   0.1280E-10
     2   0.312717026258    0.000000000000     0.4085E-04   0.8937E-06
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255569    0.000000000000     0.5585E-09   0.3332E-11
     2   0.312716446371    0.000000000000     0.2066E-04   0.5799E-06
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255574    0.000000000000     0.2650E-09   0.5073E-11
     2   0.312716407490    0.000000000000     0.1134E-04   0.3888E-07
  ------------------------------------------------------------------------

  Iteration:                 27
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255571    0.000000000000     0.8753E-10   0.3146E-11
     2   0.312716797896    0.000000000000     0.4782E-05   0.3904E-06
  ------------------------------------------------------------------------

  Iteration:                 28
  Reduced space dimension:   19

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.4113E-10   0.3942E-12
     2   0.312716704890    0.000000000000     0.2064E-05   0.9301E-07
  ------------------------------------------------------------------------

  Iteration:                 29
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.4113E-10   0.5551E-15
     2   0.312716704890    0.000000000000     0.2064E-05   0.1665E-15
  ------------------------------------------------------------------------

  Iteration:                 30
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.3003E-10   0.1855E-12
     2   0.312716711190    0.000000000000     0.9632E-06   0.6299E-08
  ------------------------------------------------------------------------

  Iteration:                 31
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2731E-10   0.1577E-13
     2   0.312716696856    0.000000000000     0.5487E-06   0.1433E-07
  ------------------------------------------------------------------------

  Iteration:                 32
  Reduced space dimension:    5

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2629E-10   0.4285E-13
     2   0.312716693064    0.000000000000     0.3535E-06   0.3792E-08
  ------------------------------------------------------------------------

  Iteration:                 33
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2632E-10   0.1471E-14
     2   0.312716702250    0.000000000000     0.3030E-06   0.9185E-08
  ------------------------------------------------------------------------

  Iteration:                 34
  Reduced space dimension:    7

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2622E-10   0.3442E-14
     2   0.312716700746    0.000000000000     0.2315E-06   0.1504E-08
  ------------------------------------------------------------------------

  Iteration:                 35
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2632E-10   0.1002E-13
     2   0.312716697137    0.000000000000     0.1339E-06   0.3609E-08
  ------------------------------------------------------------------------

  Iteration:                 36
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2640E-10   0.5551E-16
     2   0.312716699183    0.000000000000     0.6767E-07   0.2046E-08
  ------------------------------------------------------------------------

  Iteration:                 37
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2640E-10   0.6106E-15
     2   0.312716698734    0.000000000000     0.3092E-07   0.4494E-09
  ------------------------------------------------------------------------

  Iteration:                 38
  Reduced space dimension:   11

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2647E-10   0.8049E-15
     2   0.312716698694    0.000000000000     0.1082E-07   0.4011E-10
  ------------------------------------------------------------------------

  Iteration:                 39
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2583E-10   0.1477E-13
     2   0.312716698830    0.000000000000     0.4078E-08   0.1361E-09
  ------------------------------------------------------------------------

  Iteration:                 40
  Reduced space dimension:   13

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2578E-10   0.3858E-14
     2   0.312716698792    0.000000000000     0.1382E-08   0.3786E-10
  ------------------------------------------------------------------------

  Iteration:                 41
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2587E-10   0.4302E-14
     2   0.312716698773    0.000000000000     0.4344E-09   0.1931E-10
  ------------------------------------------------------------------------

  Iteration:                 42
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2588E-10   0.7133E-14
     2   0.312716698775    0.000000000000     0.1581E-09   0.1991E-11
  ------------------------------------------------------------------------

  Iteration:                 43
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255570    0.000000000000     0.2597E-10   0.7022E-14
     2   0.312716698777    0.000000000000     0.6258E-10   0.2096E-11
  ------------------------------------------------------------------------
  Convergence criterion met in 43 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247194255570
     Fraction singles (|R1|/|R|):       0.973397678083

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6       -0.964249868484
        4      6       -0.121847295829
        6      6       -0.036249387512
       13      6        0.030061485953
        1      3        0.011074241437
       22      6       -0.008603353606
       19      5        0.007863834135
        1      5       -0.007857920551
       10      6       -0.007675079118
        9      6       -0.007427820492
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.100746791319
        1      5       1      6       -0.085612058150
        1      6       5      6        0.066933008111
        1      2       1      6        0.063267573163
        1      4       2      6       -0.061512599152
        3      4       1      6       -0.054854109242
        6      5       1      6       -0.046260935197
        4      5       1      6       -0.038260256936
        7      4       1      6        0.032958804245
        4      4       2      6       -0.032734134559
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312716698777
     Fraction singles (|R1|/|R|):       0.974191508733

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.954551201855
        3      6        0.178658232265
        7      6       -0.072428436852
       14      6        0.020845095879
        2      3       -0.011040706129
       12      6       -0.005883373008
        8      2        0.005692890963
        2      5        0.005144277562
        5      4        0.004935182403
       11      4        0.003612992495
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       2      6        0.110664929267
        1      5       2      6        0.079354628525
        2      6       5      6       -0.060325722577
        2      5       1      6        0.050061414581
        3      4       2      6        0.047564612584
        1      4       1      6        0.045251222954
        1      2       2      6       -0.045012448109
        4      5       2      6        0.039742197634
        6      5       2      6        0.038941412498
        3      5       1      6        0.034289057214
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247194255570        6.726498310254
        2                  0.312716698777        8.509454805316
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.78900
     Total cpu time (sec):               1.42301

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              1.09900
     Total cpu time (sec):               2.32117

  eT terminated successfully!
