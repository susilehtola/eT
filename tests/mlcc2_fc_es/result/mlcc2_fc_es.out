


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        excited state
     end do

     memory
        available: 8
     end memory

     method
        hf
        mlcc2
     end method

     frozen orbitals
        core
     end frozen orbitals

     solver scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     solver cc gs
        energy threshold: 1.0d-11
        omega threshold:  1.0d-11
     end solver cc gs

     solver cc es
        singlet states:     2
        residual threshold: 1.0d-11
        right eigenvectors
     end solver cc es

     mlcc
        cc2 orbitals: cnto-approx
        cnto occupied cc2: 5
        cnto states: {1,2}
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             hof he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         38
     Cartesian basis functions:    40
     Primitive basis functions:    84

     Nuclear repulsion energy (a.u.):             48.518317619727
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 F     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 F     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    38
     Total number of shell pairs:           171
     Total number of AO pairs:              741

     Significant shell pairs:               161
     Significant AO pairs:                  697

     Construct shell pairs:                 171
     Construct AO pairs:                    741

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               671 /     150       0.53537E+01         194             49             32879
     2               524 /     128       0.52717E-01         398            155             81220
     3               484 /     114       0.52418E-03         303            243            117612
     4               362 /      92       0.51440E-05         311            363            131406
     5               218 /      52       0.49587E-07         221            476            103768
     6                 0 /       0       0.48539E-09         121            550                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 550

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.9691E-11
     Minimal element of difference between approximate and actual diagonal:  -0.8882E-15

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.28900
     Total cpu time (sec):               0.48389


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38
     Number of atomic orbitals:          38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:              -178.316362504245
     Number of electrons in guess:           20.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.452442672057     0.7565E-01     0.1775E+03
     2          -177.479038695767     0.1346E-01     0.2660E-01
     3          -177.480034970238     0.4311E-02     0.9963E-03
     4          -177.480161392961     0.1845E-02     0.1264E-03
     5          -177.480175069916     0.3629E-03     0.1368E-04
     6          -177.480176290419     0.7402E-04     0.1221E-05
     7          -177.480176366127     0.2207E-04     0.7571E-07
     8          -177.480176376357     0.5096E-05     0.1023E-07
     9          -177.480176377183     0.1218E-05     0.8254E-09
    10          -177.480176377209     0.2536E-06     0.2666E-10
    11          -177.480176377210     0.4399E-07     0.1052E-11
    12          -177.480176377210     0.8545E-08     0.1421E-12
    13          -177.480176377210     0.2855E-08     0.1990E-12
    14          -177.480176377210     0.1321E-08     0.8527E-13
    15          -177.480176377210     0.3976E-09     0.2558E-12
    16          -177.480176377210     0.1465E-09     0.1421E-12
    17          -177.480176377210     0.5394E-10     0.2842E-13
    18          -177.480176377210     0.4750E-11     0.8527E-13
  ---------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.600136348374
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -225.998493996937
     Total energy:                -177.480176377210

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -26.334215397000  11   0.160499885362  21   1.561381025899  31   3.061115047192
   2 -20.644676143380  12   0.548524783473  22   1.686298886878  32   3.534199119736
   3  -1.894514195174  13   0.716123096553  23   1.771732504121  33   3.762918741791
   4  -1.204323614935  14   1.106561922005  24   2.108007790821  34   3.962288761107
   5  -0.910581317587  15   1.130678945489  25   2.501253825885  35   4.000546434670
   6  -0.866359982044  16   1.195125644619  26   2.530943514170  36   4.553973955057
   7  -0.832531025218  17   1.332305214084  27   2.531186114994  37   4.580623650146
   8  -0.790125272689  18   1.423088812984  28   2.601426569358  38   5.083709277162
   9  -0.497921814630  19   1.453320735216  29   2.725832254868
  10  -0.439636463012  20   1.486906665153  30   2.888293377080
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              1.19100
     Total cpu time (sec):               2.16629

  - Preparation for frozen core approximation

     There are 2 frozen core orbitals.


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    8
     Virtual orbitals:     28
     Molecular orbitals:   36
     Atomic orbitals:      38

   - Number of ground state amplitudes:

     Single excitation amplitudes:  224


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

  Running CCS calculation for NTOs/CNTOs.

  - Summary of CCS calculation for NTOs/CNTOs:

     Wall time for CCS ground calculation (sec):                   0.03
     CPU time for CCS ground calculation (sec):                    0.06

     Wall time for CCS excited calculation (sec):                  0.09
     CPU time for CCS excited calculation (sec):                   0.27

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cnto-approx

     Number occupied cc2 orbitals:    5
     Number virtual cc2 orbitals:    15

     Number occupied ccs orbitals:    3
     Number virtual ccs orbitals:    13


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1          -177.655708396658     0.3809E-01     0.1777E+03
    2          -177.656926707442     0.9392E-02     0.1218E-02
    3          -177.657257913595     0.1994E-02     0.3312E-03
    4          -177.657326824481     0.5655E-03     0.6891E-04
    5          -177.657326337997     0.1528E-03     0.4865E-06
    6          -177.657324948255     0.3874E-04     0.1390E-05
    7          -177.657324718543     0.7979E-05     0.2297E-06
    8          -177.657324686480     0.2220E-05     0.3206E-07
    9          -177.657324678346     0.7355E-06     0.8134E-08
   10          -177.657324680715     0.3160E-06     0.2370E-08
   11          -177.657324682332     0.8988E-07     0.1617E-08
   12          -177.657324682425     0.2398E-07     0.9254E-10
   13          -177.657324682261     0.6902E-08     0.1642E-09
   14          -177.657324682212     0.1958E-08     0.4897E-10
   15          -177.657324682199     0.4907E-09     0.1290E-10
   16          -177.657324682195     0.1259E-09     0.3467E-11
   17          -177.657324682194     0.3127E-10     0.8811E-12
   18          -177.657324682194     0.8417E-11     0.8527E-13
  ---------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):  -177.657324682194

     Correlation energy (a.u.):           -0.177148304984

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      4        0.016343635509
        3      4       -0.007666605722
        1      1        0.006231676466
        4      3       -0.005406752528
       10      1        0.005240175058
        4      5       -0.005039701374
       10      4       -0.004697967164
        8      3        0.004440233732
        3      1       -0.004201011438
        5      2       -0.003958388402
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.006032069581

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.30400
     Total cpu time (sec):               0.65576


  3) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-05
     Residual threshold:              0.10E-10

     Number of singlet states:               2
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.328778710960    0.000000000000     0.4449E+00   0.3288E+00
     2   0.432098181827    0.000000000000     0.4247E+00   0.4321E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.209296619927    0.000000000000     0.5208E-01   0.1195E+00
     2   0.325255241494    0.000000000000     0.5147E-01   0.1068E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207785740593    0.000000000000     0.2382E-01   0.1511E-02
     2   0.322515681233    0.000000000000     0.3176E-01   0.2740E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207845315655    0.000000000000     0.4377E-02   0.5958E-04
     2   0.321679798071    0.000000000000     0.7567E-02   0.8359E-03
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207925767920    0.000000000000     0.1234E-02   0.8045E-04
     2   0.321651654352    0.000000000000     0.2438E-02   0.2814E-04
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207915971798    0.000000000000     0.6201E-03   0.9796E-05
     2   0.321601604125    0.000000000000     0.9403E-03   0.5005E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207910870100    0.000000000000     0.2824E-03   0.5102E-05
     2   0.321608307746    0.000000000000     0.4823E-03   0.6704E-05
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913821874    0.000000000000     0.9161E-04   0.2952E-05
     2   0.321609039449    0.000000000000     0.1822E-03   0.7317E-06
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913702574    0.000000000000     0.2280E-04   0.1193E-06
     2   0.321607155246    0.000000000000     0.6107E-04   0.1884E-05
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913547632    0.000000000000     0.5307E-05   0.1549E-06
     2   0.321608671487    0.000000000000     0.2292E-04   0.1516E-05
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913540346    0.000000000000     0.1503E-05   0.7286E-08
     2   0.321608473373    0.000000000000     0.9324E-05   0.1981E-06
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913534725    0.000000000000     0.5025E-06   0.5621E-08
     2   0.321608501982    0.000000000000     0.3228E-05   0.2861E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913531395    0.000000000000     0.1301E-06   0.3330E-08
     2   0.321608518986    0.000000000000     0.1095E-05   0.1700E-07
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530314    0.000000000000     0.3840E-07   0.1081E-08
     2   0.321608513382    0.000000000000     0.3559E-06   0.5604E-08
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530205    0.000000000000     0.1208E-07   0.1084E-09
     2   0.321608513704    0.000000000000     0.1033E-06   0.3222E-09
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530197    0.000000000000     0.3741E-08   0.8256E-11
     2   0.321608514558    0.000000000000     0.2866E-07   0.8537E-09
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   34

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530145    0.000000000000     0.8320E-09   0.5249E-10
     2   0.321608514671    0.000000000000     0.8178E-08   0.1133E-09
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530154    0.000000000000     0.1955E-09   0.9141E-11
     2   0.321608514651    0.000000000000     0.2438E-08   0.2074E-10
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   38

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530153    0.000000000000     0.4599E-10   0.2948E-12
     2   0.321608514654    0.000000000000     0.6682E-09   0.2874E-11
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530153    0.000000000000     0.1222E-10   0.2912E-13
     2   0.321608514653    0.000000000000     0.1709E-09   0.3140E-12
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   42

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530153    0.000000000000     0.4113E-11   0.1010E-13
     2   0.321608514652    0.000000000000     0.4207E-10   0.9394E-12
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   43

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.207913530153    0.000000000000     0.4114E-11   0.2054E-14
     2   0.321608514652    0.000000000000     0.9656E-11   0.1241E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.207913530153
     Fraction singles (|R1|/|R|):       0.983579244806

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5       -0.972245272264
        3      5        0.109638240703
        2      5        0.072766879999
       10      5       -0.040564078283
        1      3       -0.039734217399
        2      3        0.021251948089
        6      5       -0.020539099041
        5      3       -0.011407746409
        3      3        0.010768025954
       10      3       -0.008900757540
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.321608514652
     Fraction singles (|R1|/|R|):       0.985684447322

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      4        0.975885852349
        1      1        0.096787829670
        3      4       -0.055109452933
        4      5        0.039315553812
       10      4        0.031384971889
        3      1       -0.030751428972
        5      2        0.025651759028
        4      3        0.020871769117
        7      4        0.014971228560
        9      4       -0.013959723898
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.207913530153        5.657615327786
        2                  0.321608514652        8.751413439528
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.31600
     Total cpu time (sec):               0.73355

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.83700
     Total cpu time (sec):               1.89794

  eT terminated successfully!
