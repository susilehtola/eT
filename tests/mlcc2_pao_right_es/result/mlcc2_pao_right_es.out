


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
        right eigenvectors
     end solver cc es

     active atoms
        selection type: list
        cc2: {3}
     end active atoms

     mlcc
        cc2 orbitals: cholesky-pao
        cholesky threshold: 1.0d-1
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        3 O             cc-pvdz    cc2
     ====================================
     Total number of active atoms: 1
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.075790000000     5.000000000000        3
        2 H      0.866810000000     0.601440000000     5.000000000000        1
        3 H     -0.866810000000     0.601440000000     5.000000000000        2
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.143222342981     9.448630622825        3
        2 H      1.638033502034     1.136556880358     9.448630622825        1
        3 H     -1.638033502034     1.136556880358     9.448630622825        2
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11700
     Total cpu time (sec):               0.17137


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846800     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7373E-10
    10           -78.843851693631     0.1594E-07     0.4547E-12
    11           -78.843851693631     0.3322E-08     0.9948E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.1421E-13
    14           -78.843851693631     0.1280E-09     0.1421E-13
    15           -78.843851693631     0.1479E-10     0.1421E-13
    16           -78.843851693631     0.2905E-11     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195145   9   0.704416610867  17   1.682695299877  25   3.175416260660
   2  -1.277667044968  10   0.746260122914  18   1.804186820052  26   3.209661156313
   3  -0.898849406810  11   1.155862024383  19   1.902641648742  27   3.328173215200
   4  -0.629870222689  12   1.170770887076  20   2.148883457168  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756969  29   3.985492632586
   6  -0.485872762832  14   1.449847537220  22   2.540321860021
   7   0.159756317416  15   1.463234913438  23   2.541517310547
   8   0.229311906283  16   1.474444394527  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.45100
     Total cpu time (sec):               0.76890


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.5551E-16

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cholesky-pao

     Number occupied cc2 orbitals:    5
     Number virtual cc2 orbitals:    13

     Number occupied ccs orbitals:    1
     Number virtual ccs orbitals:    10


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.044160688177     0.3657E-01     0.7904E+02
    2           -79.045305454073     0.9064E-02     0.1145E-02
    3           -79.045492384052     0.2665E-02     0.1869E-03
    4           -79.045571807811     0.1135E-02     0.7942E-04
    5           -79.045579775786     0.3744E-03     0.7968E-05
    6           -79.045582170050     0.9856E-04     0.2394E-05
    7           -79.045582045317     0.3590E-04     0.1247E-06
    8           -79.045582083702     0.9826E-05     0.3839E-07
    9           -79.045582118491     0.3151E-05     0.3479E-07
   10           -79.045582123523     0.1515E-05     0.5033E-08
   11           -79.045582122561     0.5659E-06     0.9621E-09
   12           -79.045582124104     0.2337E-06     0.1543E-08
   13           -79.045582123441     0.6956E-07     0.6628E-09
   14           -79.045582123617     0.2039E-07     0.1760E-09
   15           -79.045582123477     0.6022E-08     0.1396E-09
   16           -79.045582123484     0.2285E-08     0.6580E-11
   17           -79.045582123500     0.6848E-09     0.1627E-10
   18           -79.045582123509     0.2617E-09     0.8569E-11
   19           -79.045582123512     0.8126E-10     0.3055E-11
   20           -79.045582123512     0.3028E-10     0.4547E-12
   21           -79.045582123512     0.1479E-10     0.1705E-12
   22           -79.045582123512     0.5019E-11     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.045582123512

     Correlation energy (a.u.):           -0.201730429881

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        5      3       -0.012293837307
        1      4        0.012108647669
        8      4        0.006278792798
        2      3       -0.006237282753
        6      4        0.005856750921
        4      5        0.005710978449
        6      2        0.005318471803
       12      3       -0.004036543512
        8      2        0.003089764537
       23      3        0.002958539743
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.006679786378

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.15200
     Total cpu time (sec):               0.30049


  3) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.448022669129    0.000000000000     0.5880E+00   0.4480E+00
     2   0.580591190772    0.000000000000     0.5772E+00   0.5806E+00
     3   0.582257862299    0.000000000000     0.5764E+00   0.5823E+00
     4   0.633142397968    0.000000000000     0.5193E+00   0.6331E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.281171626488    0.000000000000     0.1531E+00   0.1669E+00
     2   0.392130146101    0.000000000000     0.1801E+00   0.1885E+00
     3   0.393024761549    0.000000000000     0.2200E+00   0.1892E+00
     4   0.497171306491    0.000000000000     0.1662E+00   0.1360E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.266231450537    0.000000000000     0.7837E-01   0.1494E-01
     2   0.363833836714    0.000000000000     0.9134E-01   0.2830E-01
     3   0.372225925652    0.000000000000     0.9029E-01   0.2080E-01
     4   0.478000413458    0.000000000000     0.9113E-01   0.1917E-01
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264746645196    0.000000000000     0.2267E-01   0.1485E-02
     2   0.362606898788    0.000000000000     0.1063E-01   0.1227E-02
     3   0.371173949604    0.000000000000     0.2741E-01   0.1052E-02
     4   0.475634578153    0.000000000000     0.3544E-01   0.2366E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264524855398    0.000000000000     0.1255E-01   0.2218E-03
     2   0.362564966513    0.000000000000     0.3023E-02   0.4193E-04
     3   0.370304434079    0.000000000000     0.1325E-01   0.8695E-03
     4   0.473644612613    0.000000000000     0.4201E-01   0.1990E-02
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264213555632    0.000000000000     0.1808E-02   0.3113E-03
     2   0.362552070285    0.000000000000     0.6635E-03   0.1290E-04
     3   0.369997997562    0.000000000000     0.2863E-02   0.3064E-03
     4   0.468959594473    0.000000000000     0.7197E-01   0.4685E-02
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264225034941    0.000000000000     0.4934E-03   0.1148E-04
     2   0.362555163081    0.000000000000     0.1834E-03   0.3093E-05
     3   0.370075459845    0.000000000000     0.9104E-03   0.7746E-04
     4   0.460672186735    0.000000000000     0.5597E-01   0.8287E-02
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229302043    0.000000000000     0.1050E-03   0.4267E-05
     2   0.362556401469    0.000000000000     0.4430E-04   0.1238E-05
     3   0.370080442044    0.000000000000     0.2869E-03   0.4982E-05
     4   0.458493284656    0.000000000000     0.1713E-01   0.2179E-02
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229221457    0.000000000000     0.2317E-04   0.8059E-07
     2   0.362556389878    0.000000000000     0.1054E-04   0.1159E-07
     3   0.370071998483    0.000000000000     0.1346E-03   0.8444E-05
     4   0.458402140278    0.000000000000     0.3438E-02   0.9114E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072980    0.000000000000     0.4322E-05   0.1485E-06
     2   0.362556312652    0.000000000000     0.4213E-05   0.7723E-07
     3   0.370073224433    0.000000000000     0.5492E-04   0.1226E-05
     4   0.458388890351    0.000000000000     0.1021E-02   0.1325E-04
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229070318    0.000000000000     0.1301E-05   0.2661E-08
     2   0.362556342337    0.000000000000     0.1586E-05   0.2968E-07
     3   0.370074178770    0.000000000000     0.2072E-04   0.9543E-06
     4   0.458393536443    0.000000000000     0.3541E-03   0.4646E-05
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229071728    0.000000000000     0.4268E-06   0.1409E-08
     2   0.362556340215    0.000000000000     0.5628E-06   0.2121E-08
     3   0.370073919053    0.000000000000     0.7051E-05   0.2597E-06
     4   0.458391904788    0.000000000000     0.1070E-03   0.1632E-05
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072011    0.000000000000     0.7531E-07   0.2830E-09
     2   0.362556338513    0.000000000000     0.1227E-06   0.1702E-08
     3   0.370073918784    0.000000000000     0.1749E-05   0.2690E-09
     4   0.458391685532    0.000000000000     0.1994E-04   0.2193E-06
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072018    0.000000000000     0.1036E-07   0.6918E-11
     2   0.362556339094    0.000000000000     0.2680E-07   0.5803E-09
     3   0.370073924886    0.000000000000     0.3766E-06   0.6102E-08
     4   0.458391705118    0.000000000000     0.4296E-05   0.1959E-07
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.2438E-08   0.2124E-10
     2   0.362556339083    0.000000000000     0.6456E-08   0.1059E-10
     3   0.370073923162    0.000000000000     0.8766E-07   0.1724E-08
     4   0.458391735401    0.000000000000     0.7429E-06   0.3028E-07
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072042    0.000000000000     0.3492E-09   0.2823E-11
     2   0.362556339067    0.000000000000     0.1400E-08   0.1603E-10
     3   0.370073923204    0.000000000000     0.1937E-07   0.4245E-10
     4   0.458391732475    0.000000000000     0.1754E-06   0.2926E-08
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   68

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.5809E-10   0.2550E-11
     2   0.362556339072    0.000000000000     0.2268E-09   0.4775E-11
     3   0.370073923299    0.000000000000     0.3105E-08   0.9499E-10
     4   0.458391732726    0.000000000000     0.2655E-07   0.2512E-09
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   72

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6253E-11   0.1604E-13
     2   0.362556339072    0.000000000000     0.4449E-10   0.1733E-12
     3   0.370073923292    0.000000000000     0.5789E-09   0.7132E-11
     4   0.458391732801    0.000000000000     0.4837E-08   0.7550E-10
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   75

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.5678E-11   0.9215E-14
     2   0.362556339072    0.000000000000     0.4774E-11   0.1286E-12
     3   0.370073923291    0.000000000000     0.6199E-10   0.1233E-11
     4   0.458391732784    0.000000000000     0.9027E-09   0.1763E-10
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   77

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.5664E-11   0.2776E-14
     2   0.362556339072    0.000000000000     0.9656E-12   0.1499E-14
     3   0.370073923291    0.000000000000     0.1046E-10   0.4308E-13
     4   0.458391732786    0.000000000000     0.1375E-09   0.2359E-11
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   79

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.5631E-11   0.1998E-14
     2   0.362556339072    0.000000000000     0.5980E-12   0.8882E-15
     3   0.370073923291    0.000000000000     0.1860E-11   0.2620E-13
     4   0.458391732787    0.000000000000     0.1908E-10   0.4157E-12
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   80

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.5615E-11   0.1776E-14
     2   0.362556339072    0.000000000000     0.5978E-12   0.6439E-14
     3   0.370073923291    0.000000000000     0.1590E-11   0.9326E-14
     4   0.458391732787    0.000000000000     0.2888E-11   0.5640E-13
  ------------------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.264229072039
     Fraction singles (|R1|/|R|):       0.985795491396

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5       -0.951121812654
       16      5        0.220234586300
       15      5        0.108388096008
       10      5       -0.050033848087
        3      5        0.036056471793
        6      5       -0.035197550149
       13      5       -0.025759211857
        8      5        0.018750333209
       19      5        0.015301264695
        1      6       -0.011340694060
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.362556339072
     Fraction singles (|R1|/|R|):       0.991547288996

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      5        0.863355575275
       14      5        0.426521468814
        5      5        0.164105861190
       23      5        0.143170294445
       12      5       -0.077623376211
       18      5        0.043962976267
       22      5        0.012026282049
        2      6        0.009572752325
       14      6        0.007605938944
       17      2        0.006684389632
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.370073923291
     Fraction singles (|R1|/|R|):       0.988591933022

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      4       -0.930805092441
       16      4        0.236787816717
        2      3        0.138450228223
       15      4        0.116401846866
        6      4       -0.078033188958
        1      2        0.068530590946
       10      4       -0.048844741564
       14      3        0.042983474045
       13      4       -0.041511127558
        4      5        0.041473561680
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.458391732787
     Fraction singles (|R1|/|R|):       0.991662212987

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      4        0.701234450855
        1      3       -0.521166362354
       14      4        0.392293552994
        5      4        0.143454480903
       23      4        0.125058691086
       16      3        0.124997768067
       12      4       -0.081322064543
       15      3        0.059746790661
       18      4        0.036419830399
       10      3       -0.036279407805
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.264229072039        7.190039276963
        2                  0.362556339072        9.865660496486
        3                  0.370073923291       10.070224382609
        4                  0.458391732787       12.473474389230
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.42000
     Total cpu time (sec):               0.62880

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.59400
     Total cpu time (sec):               0.96611

  eT terminated successfully!
