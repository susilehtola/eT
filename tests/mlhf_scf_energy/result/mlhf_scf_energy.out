


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: 2 H2O close
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm: scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        mlhf
     end method

     active atoms
        selection type: list
        hf: {1, 2, 3}
     end active atoms


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             2 h2o close
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         48
     Cartesian basis functions:    50
     Primitive basis functions:    98

     Nuclear repulsion energy (a.u.):             37.386395233393
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        1 O             cc-pvdz    hf
        2 H             cc-pvdz    hf
        3 H             cc-pvdz    hf
     ====================================
     Total number of active atoms: 3
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O     -0.573030000000     2.189950000000    -0.052560000000        1
        2 H      0.347690000000     2.485980000000     0.050490000000        2
        3 H     -1.075800000000     3.019470000000     0.020240000000        3
        4 O     -1.567030000000    -0.324500000000     0.450780000000        4
        5 H     -1.211220000000     0.588750000000     0.375890000000        5
        6 H     -1.604140000000    -0.590960000000    -0.479690000000        6
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O     -1.082869761160     4.138405726491    -0.099324005107        1
        2 H      0.657038876250     4.697821351146     0.095412272029        2
        3 H     -2.032967364807     5.705971341340     0.038248056761        3
        4 O     -2.961257528977    -0.613216127421     0.851850742431        4
        5 H     -2.288874076596     1.112576255838     0.710329152963        5
        6 H     -3.031385265460    -1.116752550573    -0.906482724693        6
     ==============================================================================


  :: MLHF wavefunction
  =======================

  - MLHF settings:

     Occupied orbitals:    Cholesky
     Virtual orbitals:     PAOs

     Cholesky decomposition threshold:  0.10E-01

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         38
     Number of molecular orbitals:       48
     Number of atomic orbitals:          48


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a MLHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF algorithm)

   - Self-consistent field solver
  ----------------------------------

  Warning: We recommend to use the SCF-DIIS algorithm instead, which supports 
  a gradient threshold and typically converges much faster. Use only when 
  absolutely necessary!

  A Roothan-Hall self-consistent field solver. In each iteration, the 
  Roothan-Hall equation (or equations for unrestricted HF theory) are 
  solved to provide the next orbital coefficients. From the new orbitals, 
  a new density provides the next Fock matrix. The cycle repeats until 
  the solution is self-consistent (as measured by the energy change).

  - Hartree-Fock solver settings:

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:              -151.796506244372
     Number of electrons in guess:           20.000000000000

  - Active orbital space:

      Number of active occupied orbitals:        5
      Number of active virtual orbitals:        23
      Number of active orbitals:                28

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -152.011877032065     0.1544E+00     0.1520E+03
     2          -152.016473856419     0.8902E-01     0.4597E-02
     3          -152.017932021726     0.5589E-01     0.1458E-02
     4          -152.018434392605     0.3219E-01     0.5024E-03
     5          -152.018609660666     0.1974E-01     0.1753E-03
     6          -152.018671269513     0.1146E-01     0.6161E-04
     7          -152.018692936518     0.6924E-02     0.2167E-04
     8          -152.018700569224     0.4059E-02     0.7633E-05
     9          -152.018703257617     0.2431E-02     0.2688E-05
    10          -152.018704204967     0.1434E-02     0.9474E-06
    11          -152.018704538765     0.8552E-03     0.3338E-06
    12          -152.018704656396     0.5061E-03     0.1176E-06
    13          -152.018704697848     0.3011E-03     0.4145E-07
    14          -152.018704712455     0.1785E-03     0.1461E-07
    15          -152.018704717603     0.1061E-03     0.5148E-08
    16          -152.018704719417     0.6292E-04     0.1814E-08
    17          -152.018704720056     0.3737E-04     0.6393E-09
    18          -152.018704720282     0.2217E-04     0.2253E-09
    19          -152.018704720361     0.1317E-04     0.7932E-10
    20          -152.018704720389     0.7815E-05     0.2797E-10
    21          -152.018704720399     0.4640E-05     0.9862E-11
    22          -152.018704720402     0.2754E-05     0.3581E-11
    23          -152.018704720403     0.1635E-05     0.1052E-11
    24          -152.018704720404     0.9705E-06     0.3979E-12
    25          -152.018704720404     0.5762E-06     0.1421E-12
    26          -152.018704720404     0.3420E-06     0.1137E-12
    27          -152.018704720404     0.2030E-06     0.2842E-13
    28          -152.018704720404     0.1205E-06     0.0000E+00
    29          -152.018704720404     0.7155E-07     0.2842E-13
    30          -152.018704720404     0.4247E-07     0.2842E-13
    31          -152.018704720404     0.2522E-07     0.0000E+00
    32          -152.018704720404     0.1497E-07     0.8527E-13
    33          -152.018704720404     0.8886E-08     0.2842E-13
    34          -152.018704720404     0.5275E-08     0.0000E+00
    35          -152.018704720404     0.3131E-08     0.2842E-13
    36          -152.018704720404     0.1859E-08     0.5684E-13
    37          -152.018704720404     0.1104E-08     0.2842E-13
    38          -152.018704720404     0.6551E-09     0.2842E-13
    39          -152.018704720404     0.3889E-09     0.2842E-13
    40          -152.018704720404     0.2309E-09     0.5684E-13
    41          -152.018704720404     0.1370E-09     0.8527E-13
    42          -152.018704720404     0.8136E-10     0.0000E+00
    43          -152.018704720404     0.4835E-10     0.2842E-13
    44          -152.018704720404     0.2870E-10     0.5684E-13
    45          -152.018704720404     0.1706E-10     0.2842E-13
    46          -152.018704720404     0.1011E-10     0.5684E-13
    47          -152.018704720404     0.6002E-11     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 47 iterations!

  - Summary of MLHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.695204742948
     Nuclear repulsion energy:      37.386395233393
     Electronic energy:           -189.405099953797
     Total energy:                -152.018704720404

  - Summary of MLHF active/inactive contributions to electronic energy (a.u.):

     Active energy:               -104.805789875475
     Active-inactive energy:        19.262564524026
     Inactive energy:             -103.861874602348

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.597709580385   8   0.458665860143  15   1.443331554977  22   2.374619406132
   2  -1.372911488328   9   0.668896810231  16   1.463136740318  23   2.421972934984
   3  -0.730862340665  10   0.731654593339  17   1.562496405290  24   3.232245451552
   4  -0.620416745563  11   0.820592565433  18   1.605207271375  25   3.271905348926
   5  -0.538607985032  12   1.158130144437  19   1.749844436917  26   3.454691598577
   6   0.156596757916  13   1.182144316570  20   1.878493135036  27   3.822907871915
   7   0.229837619133  14   1.233002103367  21   1.926282928857  28   4.102532651957
  -----------------------------------------------------------------------------------

  - Timings for the MLHF ground state calculation

     Total wall time (sec):              3.53700
     Total cpu time (sec):               6.80225

  eT terminated successfully!
