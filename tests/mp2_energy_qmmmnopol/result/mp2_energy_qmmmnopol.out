


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O-H2O-tip3p
        charge: 0
     end system

     do
       ground state
     end do


     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     molecular mechanics
        forcefield: non-polarizable
     end molecular mechanics

     method
        hf
        mp2
     end method


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications (QM)
  ==========================================

     Name:             h2o-h2o-tip3p
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         24
     Cartesian basis functions:    25
     Primitive basis functions:    49

     Nuclear repulsion energy (a.u.):              9.307879526626
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.023452000000     0.185621000000     0.000000000000        1
        2 H      0.906315000000     1.422088000000     0.000000000000        2
        3 O      0.009319000000     1.133156000000     0.000000000000        3
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.044317857073     0.350772852968     0.000000000000        1
        2 H      1.712687132585     2.687356845030     0.000000000000        2
        3 O      0.017610357755     2.141354496408     0.000000000000        3
     ==============================================================================


  :: Molecular system specifications (MM)
  ==========================================

     Force Field:  non-polarizable

     Number of MM atoms:                   3
     Number of MM molecules:               1

    ====================================================================
                        MM Geometry (Å) and Parameters
    ====================================================================
    Atom    Mol             X          Y          Z            Charge
    ====================================================================
      O       1        -0.042964  -1.404707  -0.000000       -0.834000
      H       1        -0.419020  -1.818953   0.760190        0.417000
      H       1        -0.419020  -1.818953  -0.760190        0.417000
    ====================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-07
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    24
     Total number of shell pairs:            78
     Total number of AO pairs:              300

     Significant shell pairs:                78
     Significant AO pairs:                  300

     Construct shell pairs:                  78
     Construct AO pairs:                    300

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               285 /      71       0.47383E+01         150             29              8265
     2               220 /      60       0.46828E-01         242             99             21780
     3               141 /      47       0.46528E-03         158            157             22137
     4                87 /      21       0.45055E-05         122            226             19662
     5                 0 /       0       0.45032E-07          29            246                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 246

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.9126E-08
     Minimal element of difference between approximate and actual diagonal:  -0.8882E-15

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.07200
     Total cpu time (sec):               0.11115


  :: RHF wavefunction
  ======================

  This is a QM/MM calculation

     Electrostatic Embedding:

     Each atom of the MM portion is endowed with a charge which value 
     is a fixed external parameter.

     The QM/MM electrostatic interaction energy is defined as:

        E^ele_QM/MM = sum_i q_i * V_i(P)

     where V_i(P) is the electrostatic potential due to the QM density 
     calculated at the position of the i-th charge q_i.

     For further details, see:
     Senn & Thiel, Angew. Chem. Int. Ed., 2009, 48, 1198−1229

     CC calculation: MM charges only affect MOs and Fock


  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:         19
     Number of molecular orbitals:       24
     Number of atomic orbitals:          24


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -76.248615173566
     Number of electrons in guess:           10.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -76.335249523007     0.8894E-01     0.7634E+02
     2           -76.365696835416     0.5298E-01     0.3045E-01
     3           -76.374855089793     0.5801E-02     0.9158E-02
     4           -76.375034773860     0.1164E-02     0.1797E-03
     5           -76.375047903726     0.1470E-03     0.1313E-04
     6           -76.375048211469     0.2813E-04     0.3077E-06
     7           -76.375048221869     0.2014E-05     0.1040E-07
     8           -76.375048221944     0.5160E-06     0.7435E-10
     9           -76.375048221948     0.9069E-07     0.4817E-11
    10           -76.375048221949     0.2067E-07     0.1705E-12
    11           -76.375048221949     0.5055E-08     0.4263E-13
    12           -76.375048221949     0.1010E-08     0.1421E-13
    13           -76.375048221949     0.2780E-09     0.1421E-13
    14           -76.375048221949     0.7214E-10     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.684637098505
     Nuclear repulsion energy:       8.633980556418
     Electronic energy:            -85.009028778367
     Total energy:                 -76.375048221949

  - Summary of QM/MM energetics:
                                         a.u.             eV     kcal/mol
     QM/MM SCF Contribution:         0.324293061348
     QM/MM Electrostatic Energy:    -0.020171638955    -0.54890   -12.658

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.507914042178   7   0.324581120958  13   1.503920435793  19   2.573531286480
   2  -1.303148575054   8   0.848151806884  14   1.514231119991  20   3.319674819158
   3  -0.671524745993   9   0.904447683578  15   1.733308578906  21   3.398133782939
   4  -0.528261410381  10   1.195191602356  16   1.907573730963  22   3.550139254550
   5  -0.457614180666  11   1.233658169441  17   1.979023946237  23   3.942201817881
   6   0.227022917840  12   1.287448380718  18   2.537040755595  24   4.185418994585
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.33500
     Total cpu time (sec):               0.55869


  :: MP2 wavefunction
  ======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    5
     Virtual orbitals:     19
     Molecular orbitals:   24
     Atomic orbitals:      24

   - Number of ground state amplitudes:

     Single excitation amplitudes:  95


  :: Ground state coupled cluster engine
  =========================================

  Calculates the ground state CC wavefunction | CC > = exp(T) | R >

  This is a MP2 ground state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)


  1) Preparation of MO basis and integrals

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False


  2) Calculation of the ground state (diis algorithm)

  :: Summary of MP2 wavefunction energetics (a.u.)

     HF energy:                   -76.375048221949
     MP2 correction:               -0.202138890040
     MP2 energy:                  -76.577187111988

  - Timings for the MP2 ground state calculation

     Total wall time (sec):              0.00600
     Total cpu time (sec):               0.01128

  eT terminated successfully!
