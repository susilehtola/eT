#!/usr/bin/env python3

# provides os.path.join
import os

# provides exit
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
f = [
    get_filter(from_string='State                (Hartree)             (eV)',
               num_lines=4,
               abs_tolerance=1.0e-10,
               mask=[2]), 
    get_filter(from_string='Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >',
               num_lines=5,
               abs_tolerance=1.0e-10,
               mask=[1,2],
               ignore_sign=True),
    get_filter(from_string='Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >',
               num_lines=5,
               abs_tolerance=1.0e-10,
               mask=[3]),
    get_filter(string='Oscillator strength [a.u.]:',
               abs_tolerance=1.0e-10),
    get_filter(string='Final ground state energy (a.u.):',
               abs_tolerance=1.0e-10),
    get_filter(string='Convergence criterion met in',
               abs_tolerance=1.0e-10) 
]

# invoke the command line interface parser which returns options
options = cli()

ierr = 0

#  Run ccsd calculation, filter and compare to reference
#  - extra arguments -ks and --scratch to keep the scratch 
#    after the first calculation
#  - scratch will be in build/tests/test_name/scratch
#  - If we don't provide the "filters" line, the first output file 
#    would not be filtered and compared
#  - We can also provide a second filter list e.g. g to check both 
#    output files for different things
#
for inp in ['ccsd_eom_1_state.inp']:
    #
    # the run function runs the code with extra arguments
    # keep the scratch in the tests folder in build
    #
    ierr += run(options,
                configure,
                input_files=inp,
                extra_args='-save .',
                filters={'out': f}) 

# reset ierr: helpful if the first calculation is supposed to crash
# e.g. if the calculations has to little iterations to converge
ierr = 0
#
#  Run CC3 calculation with the same filter list f
#  We could also provide a different filter list g
#
for inp in ['restart_ccsd_2_states_from_1.inp']:
    #
    # the run function runs the code and filters the outputs
    # don't provide -ks to delete scratch after succesful run
    #
    ierr += run(options,
                configure,
                input_files=inp,
                extra_args='-load ./ccsd_eom_1_state_restart_files',
                filters={'out': f})

sys.exit(ierr)
