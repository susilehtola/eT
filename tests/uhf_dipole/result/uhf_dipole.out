


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
        multiplicity: 3
     end system

     print
        output print level: verbose
     end print

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     do
        ground state
     end do

     hf mean value
        dipole
     end hf mean value


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             hof he
     Charge:           0
     Multiplicity:     3
     Coordinate units: angstrom

     Pure basis functions:         38
     Cartesian basis functions:    40
     Primitive basis functions:    84

     Nuclear repulsion energy (a.u.):             48.393468434740
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 F     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 F     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================


  :: UHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of alpha electrons:              11
     Number of beta electrons:                9
     Number of virtual alpha orbitals:       27
     Number of virtual beta orbitals:        29
     Number of molecular orbitals:           38
     Number of atomic orbitals:              38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a UHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Calculate dipole and/or quadrupole moments


  1) Generate initial SAD density
     Generated atomic density for H  using UHF/cc-pvdz
     Generated atomic density for F  using UHF/cc-pvdz
     Generated atomic density for O  using UHF/cc-pvdz
     Generated atomic density for He using UHF/cc-pvdz


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-11
     Gradient threshold:            0.1000E-11

     Coulomb screening threshold:   0.1000E-17
     Exchange screening threshold:  0.1000E-15
     Fock precision:                0.1000E-35
     Integral cutoff:               0.1000E-17

  - Setting initial AO density to SAD

     Energy of initial guess:              -178.512758938725
     Number of electrons in guess:           20.000000000000

  Storing DIIS records in memory.

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.235968989740     0.4858E-01     0.1772E+03
  Switching to Fock construction using density differences.
     2          -177.302484955278     0.2286E-01     0.6652E-01
     3          -177.312278763610     0.1139E-01     0.9794E-02
     4          -177.314566931400     0.5468E-02     0.2288E-02
     5          -177.315268480120     0.4074E-02     0.7015E-03
     6          -177.315524835530     0.9855E-03     0.2564E-03
     7          -177.315558065043     0.2063E-03     0.3323E-04
     8          -177.315560276710     0.7971E-04     0.2212E-05
     9          -177.315560552006     0.2714E-04     0.2753E-06
    10          -177.315560575789     0.8356E-05     0.2378E-07
    11          -177.315560580026     0.6874E-05     0.4237E-08
    12          -177.315560582620     0.5377E-05     0.2594E-08
    13          -177.315560585541     0.2744E-05     0.2922E-08
    14          -177.315560586534     0.7069E-06     0.9926E-09
    15          -177.315560586608     0.2950E-06     0.7353E-10
    16          -177.315560586616     0.1134E-06     0.8413E-11
    17          -177.315560586617     0.3363E-07     0.7105E-12
    18          -177.315560586617     0.1966E-07     0.0000E+00
    19          -177.315560586617     0.7720E-08     0.2842E-13
    20          -177.315560586617     0.3048E-08     0.1421E-12
    21          -177.315560586617     0.1484E-08     0.2558E-12
    22          -177.315560586617     0.3459E-09     0.1705E-12
    23          -177.315560586617     0.1818E-09     0.1705E-12
    24          -177.315560586617     0.1426E-09     0.1137E-12
    25          -177.315560586617     0.9839E-10     0.5684E-13
    26          -177.315560586617     0.4202E-10     0.0000E+00
    27          -177.315560586617     0.1574E-10     0.8527E-13
    28          -177.315560586617     0.6074E-11     0.2842E-13
    29          -177.315560586617     0.2016E-11     0.2842E-13
    30          -177.315560586617     0.4684E-12     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 30 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.645902330182
     HOMO-LUMO gap (beta):           0.647935076416
     Nuclear repulsion energy:      48.393468434740
     Electronic energy:           -225.709029021357
     Total energy:                -177.315560586617

  - Alpha orbital energies

  -----------------------------------------------------------------------------------
   1 -26.495816002829  11  -0.214962507774  21   1.523365254139  31   2.887233868503
   2 -20.818349957896  12   0.430939822408  22   1.596127129400  32   3.357637315664
   3  -2.057453733898  13   0.658428674344  23   1.710223597666  33   3.643188307028
   4  -1.384999729402  14   0.958336542567  24   2.053653113005  34   3.818738904460
   5  -1.024809894233  15   1.004765365318  25   2.425235766977  35   3.855920507052
   6  -1.021290147827  16   1.071260092378  26   2.510882858760  36   4.392892265277
   7  -0.940033879762  17   1.255207028832  27   2.511740885644  37   4.454793937983
   8  -0.927063112601  18   1.350781563641  28   2.537624588066  38   4.948067289076
   9  -0.658687059702  19   1.353818737715  29   2.550012294702
  10  -0.656447203933  20   1.411285512956  30   2.761115579754
  -----------------------------------------------------------------------------------

  - Beta orbital energies

  -----------------------------------------------------------------------------------
   1 -26.481967826905  11   0.226482585936  21   1.592271839186  31   2.997279409375
   2 -20.762131289522  12   0.474044064477  22   1.645772080774  32   3.450656471972
   3  -1.998105864831  13   0.784675252555  23   1.754383283127  33   3.680030440798
   4  -1.250954525100  14   1.046718341766  24   2.137903737871  34   3.857133606587
   5  -0.966559252520  15   1.105138377269  25   2.444405740756  35   3.908351927824
   6  -0.929286341553  16   1.120668692565  26   2.511411624478  36   4.445071072051
   7  -0.888615067253  17   1.271517100516  27   2.512153498863  37   4.466835864341
   8  -0.830120600381  18   1.368679848294  28   2.550611313122  38   4.967553674884
   9  -0.591447279318  19   1.414269757545  29   2.639175919347
  10   0.056487797098  20   1.438713004764  30   2.798012281379
  -----------------------------------------------------------------------------------


  3) Calculate dipole and/or quadrupole moments

  - Operator: dipole moment [a.u.]

      Comp.   Electronic        Nuclear          Total
     ------------------------------------------------------
        x    0.1178573E+02  -0.1272632E+02  -0.9405943E+00
        y   -0.1054950E+02   0.1014420E+02  -0.4052955E+00
        z   -0.1985172E+03   0.1985346E+03   0.1746230E-01
     ------------------------------------------------------

     x:         -0.9405943
     y:         -0.4052955
     z:          0.0174623

     |mu|:       1.0243471

  - Operator: dipole moment [Debye]

      Comp.   Electronic        Nuclear          Total
     ------------------------------------------------------
        x    0.2995633E+02  -0.3234709E+02  -0.2390752E+01
        y   -0.2681415E+02   0.2578399E+02  -0.1030158E+01
        z   -0.5045803E+03   0.5046247E+03   0.4438474E-01
     ------------------------------------------------------

     x:         -2.3907524
     y:         -1.0301584
     z:          0.0443847

     |mu|:       2.6036308

  - Timings for the UHF ground state calculation

     Total wall time (sec):              4.84100
     Total cpu time (sec):               9.00729

  eT terminated successfully!
